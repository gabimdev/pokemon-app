import { Footer } from './models/footer';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public footer:Footer;
  constructor() {
    this.footer={
      contact:"mipagina@mipagina.com",
      rights:"Esta pagina esta protegida por derechos de autor"
    }
  }

  ngOnInit(): void {
  }

}
