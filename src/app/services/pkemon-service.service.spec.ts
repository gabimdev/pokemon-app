import { TestBed } from '@angular/core/testing';

import { PkemonServiceService } from './pkemon-service.service';

describe('PkemonServiceService', () => {
  let service: PkemonServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PkemonServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
