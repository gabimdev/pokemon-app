import { Injectable } from '@angular/core';
import { Registration } from '../pages/home/registration/models/registration';

@Injectable({
  providedIn: 'root'
})
export class UserListService {

  public userList:Registration[];

  constructor() {
    this.userList =[
      {
        name:'User',
        nickname:'User nickname',
        email:'user@user.com',
        password:'user password'
      }
    ]
   }

  public getUser(): Registration[]{
    return this.userList;
  }

  public postUser(user: Registration){
    this.userList.push(user)
  }
}
