import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class PkemonServiceService {

  public baseUrl: string = 'https://pokeapi.co/api/v2/pokemon/';
  public pokemonName: string = '';
  public pokemonId: number | null = null;
  public pokemonList: string ="https://pokeapi.co/api/v2/pokemon?limit=100&offset=200";
  constructor(private http: HttpClient) { }

  public getPokemonByName(name: string){
   if (name === undefined) {
    return
   }
   this.pokemonName = name;
   return this.http.get(this.baseUrl+this.pokemonName);
  }

  public getRandomPokemon(id: number) {
    if (id === undefined) {
      return
    }
    this.pokemonId = id
    return this.http.get(this.baseUrl + this.pokemonId)
  }

  public getPokemonList(){
    return this.http.get(this.pokemonList)
  }


}
