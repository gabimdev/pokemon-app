import { PkemonServiceService } from './../../../services/pkemon-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrls: ['./list-pokemon.component.scss']
})
export class ListPokemonComponent implements OnInit {

  public pokemonListData:any;
  public pokemonListToPrint:any;
  public pokemonSearchData:any;
  constructor(public pkemonService: PkemonServiceService) { }

  ngOnInit(): void {
  }

 public getPokemonList(){
   this.pkemonService.getPokemonList().subscribe((data:any)=>{
      this.pokemonListData=data;
      this.pokemonListToPrint = this.pokemonListData.results.map(({name}) => ({name}));
    })
   console.log(this.pokemonListToPrint);
    return this.pokemonListToPrint;
  }
  public getPokemonByName( name:string){
    this.pkemonService.getPokemonByName(name).subscribe((data:any)=>{
      this.pokemonSearchData= data;
    })
    return this.pokemonSearchData;
  }

}
