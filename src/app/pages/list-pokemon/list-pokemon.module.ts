import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListPokemonRoutingModule } from './list-pokemon-routing.module';
import { ListPokemonComponent } from './list-pokemon-component/list-pokemon.component';
import { PokemonDetailsComponent } from './pokemon-details/pokemon-details.component';


@NgModule({
  declarations: [ListPokemonComponent, PokemonDetailsComponent],
  imports: [
    CommonModule,
    ListPokemonRoutingModule
  ]
})
export class ListPokemonModule { }
