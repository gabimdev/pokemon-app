import { PasswordValidatorService } from './../../../services/password-validator.service';
import { UserListService } from '../../../services/user-list.service';
import { Registration } from './models/registration';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  public submitted:boolean = false;
  public userRegistration: FormGroup;


  constructor(
    private formBuilder:FormBuilder,
    public userListService:UserListService,
    public passwordValidator: PasswordValidatorService
    ) {
    this.userRegistration = this.formBuilder.group({
      name:['',[Validators.required, Validators.minLength(5)]],
      nickname:['',[Validators.required, Validators.minLength(2)]],
      email:['',[Validators.required, Validators.email]],
      password:['',[Validators.required, Validators.minLength(8)]],
      passwordRepeat:['',[Validators.required, Validators.minLength(8)]],
    },
    {
      validator: passwordValidator.comparePassword('password', 'passwordRepeat')
    });
  }

  ngOnInit(): void {
  }

  public onSubmit(): void{
    this.submitted =true;
    if (this.userRegistration.valid) {
      const USER: Registration ={
        name: this.userRegistration.get('name').value,
        nickname: this.userRegistration.get('nickname').value,
        email: this.userRegistration.get('email').value,
        password: this.userRegistration.get('password').value,
      }
      this.userListService.postUser(USER);
      this.userRegistration.reset();
      this.submitted = false;
    };

    }
  }
