import { UserListService } from '../../../../services/user-list.service';
import { Registration } from './../models/registration';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registred-user',
  templateUrl: './registred-user.component.html',
  styleUrls: ['./registred-user.component.scss']
})
export class RegistredUserComponent implements OnInit {

  public userRegistred:Array <Registration| null> = [];
  constructor(public userListService:UserListService) {
    this.userRegistred = userListService.getUser();
   }

  ngOnInit(): void {
  }

}
