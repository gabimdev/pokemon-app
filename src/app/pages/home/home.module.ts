import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home-component/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { RegistredUserComponent } from './registration/registred-user/registred-user.component';


@NgModule({
  declarations: [HomeComponent, RegistrationComponent, RegistredUserComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    ReactiveFormsModule,
  ]
})
export class HomeModule { }
