import { PkemonServiceService } from './../../../services/pkemon-service.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-pokemon',
  templateUrl: './search-pokemon.component.html',
  styleUrls: ['./search-pokemon.component.scss']
})
export class SearchPokemonComponent implements OnInit {

  public pokemonSearchData:any;
  constructor(public pkmemonService: PkemonServiceService) {}

  ngOnInit(): void {
  }

  public getPokemonByName( name:string){
    this.pkmemonService.getPokemonByName(name).subscribe((data:any)=>{
      this.pokemonSearchData= data;
    })
    return this.pokemonSearchData;
  }



}
