import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchPokemonRoutingModule } from './search-pokemon-routing.module';
import { SearchPokemonComponent } from  './search-pokemon-component/search-pokemon.component';


@NgModule({
  declarations: [SearchPokemonComponent],
  imports: [
    CommonModule,
    SearchPokemonRoutingModule
  ]
})
export class SearchPokemonModule { }
