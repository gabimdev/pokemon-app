import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
    {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomeModule)
    },
    {
    path: 'searchPokemon',
    loadChildren: () => import('./pages/search-pokemon/search-pokemon.module').then(m => m.SearchPokemonModule)
    },
    {
    path: 'listPokemon',
    loadChildren: () => import('./pages/list-pokemon/list-pokemon.module').then(m => m.ListPokemonModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
