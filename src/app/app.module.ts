import { PkemonServiceService } from './services/pkemon-service.service';
import { UserListService } from './services/user-list.service';
import { HeaderComponent } from './components/header/header-component/header.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/footer/footer.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    UserListService,
    PkemonServiceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
